---
layout: default
---
Övernattningsrum
================
Föreningen har ett rum som man kan hyra någon/några
dagar om behov finns av extra sovplatser. Har du exempelvis gästbesök
och ont om plats kan du använda rummet. Pälsdjur får vistas
i rummet.

Vad finns i rummet?
-------------------
I rummet finns
* 4 sängplatser
* pentry
* toalett
* dusch

<a href="bilder.htm#uthyr" target="_blank">Se bilder</a>

Hur man får tillträde till rummet
---------------------------------

Tag kontakt med styrelsen för bokning av rummet. Hyran för rummet
erläggs i efterskott via Swish.

- vardagar 350 kr natt måndag-torsdag
- helg 400 kr natt fredag-söndag

Om du behöver boka av
---------------------
För att så många som möjligt ska ha kunna använda
rummet är det viktigt att du bokar av senast 24 timmar innan din bokade
dag, gärna 3 dagar innan om du av någon anledning inte behöver
rummet. Om du inte bokar av i tid betalar du på samma sätt som om
du använt
rummet. Boka av genom att kontakta styrelsen.

Övernattningsvärd
-----------------
* Jeanette Rudels <a href="mailto:jeanette_rudels@hotmail.com">jeanette_rudels@hotmail.com</a>, eller <a href="mailto:styrelsen@vimpeln16.se">styrelsen@vimpeln16.se</a>


Verkstadsrum
============
Föreningen har ett verkstadsrum i källarkorridoren mellan
A- och B-portarna. Där man kan snickra och måla mindre
saker. Det finns lite verktyg, borrmaskin, geringssåg, hyvelbänk
och en del andra bra möjligheter.

Hur man får tillträde till verkstaden
-------------------------------------
Boka tid i almanackan på verkstadsrumsdörren. Din ytterdörrnyckel
fungerar till rummet.

När du är klar med ditt arbete
------------------------------
När du är klar med ditt arbete i verkstaden skall du städa och
ta med dina sopor, färgburkar och spillvirke därifrån så att
nästa granne möts av en snygg verkstad.

Mötesrum
========
Föreningen har ett mötesrum i tvättstugekorridoren som är möblerat
med bord och stolar. Rummet kan användas till möten, för att arbeta
ostört en stund eller äta tillsammans om man är många.

Hur man får tillträde till mötesrummet
--------------------------------------
Boka tid i almanackan på mötesrumsdörren. Din ytterdörrnyckel
fungerar till rummet.

När du är klar med ditt besök
-----------------------------
När du är klar med ditt besök i mötesrummet skall du ställa i ordning,
städa och ta med dina sopor därifrån så att nästa granne möts
av ett snyggt rum.

![](bilder/verkstan.gif)
