---
layout: default
---
Bredband
========
Vår förening
äger fastighetsnätet och köper anslutning 
genom leverantören Ownit. Föreningen har avtalat om en gruppanslutning
för att få en förmånlig anslutningsavgift för
medlemmarna, och bredband och avgift är obligatorisk för
alla lägenheter.

Om du vill ansluta dig eller avsluta din inkoppling
------------------------
För aktivering av bredband och andra tjänster, följ instruktionen
<a href="https://www.ownit.se/kundservice/guider/abonnemang/aktivering/aktivering-av-bredband-gruppanslutning" target="_blank">Aktivering
hos Ownit</a>.

Koppla in en dator eller router till anslutningsdosan
ovanför din entrédörr alternativt i bredbandsskåpet.
Den utrustning du kopplar in till dosan ska vara inställd för
att hämta "Dynamisk IP-adress". Routern behöver inte
komma från Ownit.

Om du aktiverat bredbandet har du också ingått ett avtal
med vår leverantör, och är därmed juridiskt
ansvarig för hur ditt bredband används. Av det skälet är
det viktigt att du säger upp ditt bredbandsabonnemang till vår
leverantör om du flyttar från vår förening.

Krav för anslutning
-------------------
Ett krav för anslutning är du har har ett aktivt och
uppdaterat antivirusprogram i din dator. Om din dator sprider virus
stängs din anslutning av. Om du dessutom har en aktiv brandvägg
och ser till att uppdatera ditt operativsystem efter leverantörens
rekommendationer får du en mer problemfri bredbandsanvändning.

Fakta
-----
* fiber till fastigheten och eget Ethernet-nät
* upp till 1000 Mb i båda riktningar, beroende på din utrustning
* en IP-adress per hushåll
* 10 e-postadresser, öppnas av dig hos <a href="https://www.ownit.se" target="_blank">www.ownit.se</a>
* 100 Mb hemsidesutrymme
* support från Ownits kundtjänst
* tillgång till IP-telefoni och IP-tv mot extra avgift

<a href="http://www.ownit.se" target="_blank">Till
  Ownit</a> , vår leverantör

Kabel-TV, analog och digital
========

Föreningen har ett kollektivavtal
för analog/digital kabel-tv, vilket innebär att alla lägenheter
har tillgång till ett basutbud  som Telenor
Kabel-tv levererar. Basutbudet ingår
i avgiften till föreningen. Du kan
även beställa ett utökat utbud av digital-/hd-tv hos Telenor.
Ett sådant
abonnemang betalar du själv.

Om du felanmäler
kabel-tv och en utkallad tekniker hittar felet i din
egen utrustning kan du få betala en avgift till Telenor.

<a href="http://www.bredbandsbolaget.se/kundservice/index.html" target="_blank">Telenor kabel-tv</a><br>
<a href="dokument/TV_Kanal_Analog_211103.pdf" target="_blank">Kanallista
 analog/digital kabel-tv basutbud</a>

Behov av bredbandssupport
-------------------------
Kontakta Ownit:s helpdesk. Du hittar öppettider och telefonnummer
hos <a href="https://www.ownit.se/kundservice" target="_blank">Ownit</a>.

Tänk på att om det behövs hembesök för att lösa ditt behov
kan det utgå en extra avgift om supportbehovet beror på problem i din
egen utrustning, som dator eller program.

![](bilder/bredband.gif)
